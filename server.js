"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const fs = require('fs');
const hbs = require('hbs');
let port = process.env.PORT || 5000;
const app = express_1.default();
hbs.registerPartials(__dirname + '/views/partials');
app.set('view engine', 'hbs');
app.set("views", __dirname + "/views");
app.use(express_1.default.static(__dirname + '/public'));
app.use((req, res, next) => {
    let now = new Date().toString();
    let log = `${now}: ${req.method} ${req.url}`;
    fs.appendFile('server.log', log + '\n', (err) => {
        err && console.log('Unable to append to server.log');
    });
    next();
});
// app.use((req: Request, res: Response, next: NextFunction) => {
//     res.render('maintenance.hbs', {
//         pageTitle: 'Maintenance page',
//         currentYear: new Date().getFullYear()
//     })
// })
app.get('/', (req, res) => {
    res.render('home.hbs', {
        pageTitle: 'Home page',
        currentYear: new Date().getFullYear(),
        welcomeMessage: 'Hello to Home'
    });
});
app.get('/about', (req, res) => {
    res.render('about.hbs', {
        pageTitle: 'About page',
        currentYear: new Date().getFullYear()
    });
});
app.get('/bad', (req, res) => {
    res.send('Bad');
});
app.listen(port, () => console.log(`Server running on port ${port}`));
