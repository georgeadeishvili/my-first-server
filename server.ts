import express, { Application, Request, Response, NextFunction } from 'express'

const fs = require('fs')
const hbs = require('hbs')

let port = process.env.PORT || 5000
const app: Application = express()

hbs.registerPartials(__dirname + '/views/partials')
app.set('view engine', 'hbs')
app.set("views", __dirname + "/views");
app.use(express.static(__dirname + '/public'))

app.use((req: Request, res: Response, next: NextFunction) => {
    let now = new Date().toString()
    let log = `${now}: ${req.method} ${req.url}`
    fs.appendFile('server.log', log + '\n', (err: Error) => {
        err && console.log('Unable to append to server.log')
    })
    next()
})

// app.use((req: Request, res: Response, next: NextFunction) => {
//     res.render('maintenance.hbs', {
//         pageTitle: 'Maintenance page',
//         currentYear: new Date().getFullYear()
//     })
// })

app.get('/', (req: Request, res: Response) => {
    res.render('home.hbs', {
        pageTitle: 'Home page',
        currentYear: new Date().getFullYear(),
        welcomeMessage: 'Hello to Home'
    })
})

app.get('/about', (req: Request, res: Response) => {
    res.render('about.hbs', {
        pageTitle: 'About page',
        currentYear: new Date().getFullYear()
    })
})

app.get('/bad', (req: Request, res: Response) => {
    res.send('Bad')
})

app.listen(port, () => console.log(`Server running on port ${port}`))
